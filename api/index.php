<?php
/*Файл для роутинга АПИ*/
//header('Content-Type: application/json; charset=utf-8');
require 'Slim/Slim.php';
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'init.php';

\Slim\Slim::registerAutoloader();

/*$app = new \Slim\Slim (
    array(
        'debug' => true,
        'log.enabled' => true,
        'log.level' => \Slim\Log::DEBUG	
    )
);
*/
$app = new \Slim\Slim ();

$app->add(new \Slim\Middleware\ContentTypes());

$result_obj = Api\ResultApi::getInstance();	

$req_ip = $app->request->getIp();

//Переопределяем 404 ошибку
$app->notFound(function () use ($app, $result_obj) {
    $result = $result_obj->resultErrorHTTP (404, "Not Found", 404);
	$app->response->setStatus ( $result['responseCode']);
	echo json_encode($result, JSON_UNESCAPED_UNICODE);	
});


// GET route
$app->get( '/', function () use ($app) {
	    
        $app->response->headers->set('Content-Type', 'application/json');
	   	echo "API Sechenov clinic ";
		
		print_r ($app->response);
		exit;
		
		
		}
);



$app->get('/redirectApp', 

	function () use ($app, $result_obj) {
        
            echo '<script type="text/javascript">
                     document.location.href="sechenogram://open"
                        </script>';
        
	}
);





/*Группа для роутинга пользователя*/
$app->group('/user', 
	function () use ($app, $result_obj) {

        //$app->response->headers->set('Content-Type', 'application/json');

		/*Авторизация через email*/
		$app->post('/Login',
			function () use ($app, $result_obj) {
				try {
				    $post = $result_obj->postJson();
				    $obj = new Auth\Auth (); 
				    if (json_last_error() != 0) {
                        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidRequest'], 400);
						$app->response->setStatus ( $result['responseCode']);
                        echo json_encode($result, JSON_UNESCAPED_UNICODE);
                        exit;
                    } 
				    
					if (isset($post['email']) && preg_match(REG_EMAIL, $post['email'])) {	
					    if (!isset($post['password'])) { 
					        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidParams'], 400);
						    $app->response->setStatus ( $result['responseCode']);
					    } else {
					        $email = $post['email'];
						    /*Пока берем весь post-мссив и сразу кидаем его в метод по регистрации*/
						    $pass = $post['password'];	
						    $result = $obj->authUser($email, $pass);
						    $app->response->setStatus ($result['responseCode']);
					    }
					} else {
						$result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidEmail'], 400);
						$app->response->setStatus ( $result['responseCode']);
					}
				} catch (Exception $e){			
					$result = $result_obj->internalErrorException ($e);
					$app->response->setStatus ( $result['responseCode']);
				}
				echo json_encode($result, JSON_UNESCAPED_UNICODE);			
			}	
		);
		
		
		/*Регистрация */
		$app->post('/Register',
			function () use ($app, $result_obj) {	
				try {	
                    $post = $result_obj->postJson();
                    $obj = new Auth\Register (); 
                    if (json_last_error() != 0) {
                        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidRequest'], 400);
						$app->response->setStatus ( $result['responseCode']);
                        echo json_encode($result, JSON_UNESCAPED_UNICODE);
                        exit;
                    } 
                   
					if (isset($post['email']) && preg_match(REG_EMAIL, $post['email'])) {		
						/*Пока берем весь post-мссив и сразу кидаем его в метод по регистрации*/
						 $email = $post['email'];
						$result = $obj->registerUser($post);
						$app->response->setStatus ($result['responseCode']);
					} else {
						$result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidEmail'], 400);
						$app->response->setStatus($result['responseCode']);
					}
				} catch (Exception $e){			
					$result = $result_obj->internalErrorException ($e);
					$app->response->setStatus($result['responseCode']);
				}
				echo json_encode($result, JSON_UNESCAPED_UNICODE);		
			}
		
		);
		
		/*Подтверждение регистрации*/
		$app->get('/verify',
		    function () use ($app, $result_obj) {
		        try {
		            $token = $app->request->get('token');
		            $obj = new Auth\Register(); 
					$result = $obj->checkRegisteredEmail($token);
					
					
					echo $result;
		            exit;
		        } catch (Exception $e) {
		            if (is_a($e, "Firebase\JWT\SignatureInvalidException")
		                 or is_a ($e, "ErrorException")
		                 or is_a ($e, "UnexpectedValueException"))
		             {
				        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
						$app->response->setStatus ( $result['responseCode']);
    					$app->response->setStatus(400);	
				    } else {
    					$result = $result_obj->internalErrorException ($e);
    					$app->response->setStatus($result['responseCode']);		
				    }							
				}
				echo json_encode($result, JSON_UNESCAPED_UNICODE);
		    }
		);
		
		
		/*Метод повторной активации пользователя по почте*/
		$app->get('/reActivate',
		    function () use ($app, $result_obj) {
		        try {
		            $token = $app->request->headers->get('Token');
		            $obj = new Auth\Register(); 
		            
		            if(!empty($token)) {
		                $result = $obj->reActivate ($token);
					    $app->response->setStatus ($result['responseCode']);
		            } else {
		                $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
					    $app->response->setStatus ( $result['responseCode']);
		            }
		        } catch (Exception $e) {
		             
		            if (is_a($e, "Firebase\JWT\SignatureInvalidException")
		                 or is_a ($e, "UnexpectedValueException")
		                 or is_a ($e, 'DomainException'))
		             {
				        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
						$app->response->setStatus ( $result['responseCode']);
    					$app->response->setStatus(400);	
				    } else {
    					$result = $result_obj->internalErrorException ($e);
    					$app->response->setStatus($result['responseCode']);		
				    }	
		        }
		        
		        echo json_encode($result, JSON_UNESCAPED_UNICODE);
		    }
		);
		
		
		/*Активация аккаунта пользователя*/
		$app->post('/CheckEmailAvailability',
			function () use ($app, $result_obj) {
				try {
					$post = $result_obj->postJson();
                    $obj = new Auth\Base (); 
                    
                    if (json_last_error() != 0) {
                        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidRequest'], 400);
						$app->response->setStatus ( $result['responseCode']);
                        echo json_encode($result, JSON_UNESCAPED_UNICODE);
                        exit;
                    } 
                    
					if (isset($post['email']) && preg_match(REG_EMAIL,  $post['email'])) {
					    $email = $post['email'];
    					$result = $obj->checkEmail( $email );					
    					$app->response->setStatus ( $result['responseCode'] );	
					} else {
					    $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidEmail'], 400);
						$app->response->setStatus ( $result['responseCode']);
					}
				} catch (Exception $e) {
					$result = $result_obj->internalErrorException ($e);
					$app->response->setStatus ( $result['responseCode'] );					
				}
				echo json_encode($result, JSON_UNESCAPED_UNICODE);
			}
		);
		
		
		/*Восстановление пароля пользователя*/
		$app->post('/RestorePassword',
			function () use ($app, $result_obj) {
				try {
				    $post = $result_obj->postJson();
				    $obj = new Auth\Auth ();
				    if (json_last_error() != 0) {
                        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidRequest'], 400);
						$app->response->setStatus ( $result['responseCode']);
                        echo json_encode($result, JSON_UNESCAPED_UNICODE);
                        exit;
                    }
					if (isset($post['email']) && preg_match(REG_EMAIL, $post['email'])) {
					    $email = $post['email'];
					    $result = $obj->restorePassword ($email);
					    $app->response->setStatus ($result['responseCode']);
					} else {
					    $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidEmail'], 400);
						$app->response->setStatus ( $result['responseCode']);
					}
				} catch (Exception $e) {
					$result = $result_obj->internalErrorException ($e);
					$app->response->setStatus ( $result['responseCode'] );					
				}
				echo json_encode($result, JSON_UNESCAPED_UNICODE);	
			}
		);
		
		
		/*Изменить пароль*/
		$app->post('/ChangePassword',
		    function () use ($app, $result_obj) {
		        try {	
		            $post = $result_obj->postJson();
		            $obj = new Auth\Auth ();
		            if (json_last_error() != 0) {
                        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidRequest'], 400);
						$app->response->setStatus ( $result['responseCode']);
                        echo json_encode($result, JSON_UNESCAPED_UNICODE);
                        exit;
                    }
		            
		            $token = $app->request->headers->get('Token');
				    if (!empty($token)) {
					    $result = $obj->changePassword ($post, $token);
					    $app->response->setStatus ($result['responseCode']);
				    } else {
				        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
					    $app->response->setStatus ( $result['responseCode']);
				    }

				} catch (Exception $e) {
					if ( is_a($e, "Firebase\JWT\SignatureInvalidException")
		                 or is_a($e, "UnexpectedValueException")
		             ) {
				        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
						$app->response->setStatus ( $result['responseCode']);
    					$app->response->setStatus(400);	
				    } else {
    					$result = $result_obj->internalErrorException ($e);
    					$app->response->setStatus($result['responseCode']);		
				    }					
				}
				echo json_encode($result, JSON_UNESCAPED_UNICODE);	
		    }
		);
		
		/*Изменить номер телефонга*/
		$app->post('/ChangePhone',
		    function () use ($app, $result_obj) {
		        try {	
		            
		            echo "Change Phone";
		            exit;
		            
					/*$email = $app->request->post('email');
					$password = $app->request->post('password');
					$obj = new Auth\Auth ();
					$result = $obj->restorePassword ($email, $password);*/
					$app->response->setStatus ($result['responseCode']);	
				} catch (Exception $e) {
					$result = $result_obj->internalErrorException ($e);
					$app->response->setStatus($result['responseCode']);					
				}
				echo json_encode($result, JSON_UNESCAPED_UNICODE);
		    }
		);
	
		/*Метод MedicalInfo*/
		$app->get('/MedicalInfo',
		    function () use ($app, $result_obj) {
		        try {	
		            $token = $app->request->headers->get('Token');
                    $obj = new Auth\User (); 
		            if (!empty($token)) {
		                $result = $obj->getMedicalInfo($token);
		                $app->response->setStatus ($result['responseCode']);
		            } else {
		                $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
						$app->response->setStatus ( $result['responseCode']);
		            }
					$app->response->setStatus ($result['responseCode']);	
				} catch (Exception $e) {
				    if (is_a($e, "Firebase\JWT\SignatureInvalidException")
		                 or is_a ($e, "ErrorException")
		                 or is_a ($e, "UnexpectedValueException"))
		             {
				        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
						$app->response->setStatus ( $result['responseCode']);
    					$app->response->setStatus(400);	
				    } else {
    					$result = $result_obj->internalErrorException ($e);
    					$app->response->setStatus($result['responseCode']);		
				    }			
				}
				echo json_encode($result, JSON_UNESCAPED_UNICODE);
		    }
		);
		
		
		/*Метод MedicalInfo*/
		$app->get('/MedTypes',
		    function () use ($app, $result_obj) {
		        try {	
		            $token = $app->request->headers->get('Token');
		            $get = $app->request->get();
                    $obj = new Auth\User (); 
		            if (!empty($token)) {
		                $result = $obj->getMedTypes($token, $get);
		                $app->response->setStatus ($result['responseCode']);
		            } else {
		                $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
						$app->response->setStatus ( $result['responseCode']);
		            }
					$app->response->setStatus ($result['responseCode']);	
				} catch (Exception $e) {
					if (is_a($e, "Firebase\JWT\SignatureInvalidException")
		                 or is_a ($e, "ErrorException")
		                 or is_a ($e, "UnexpectedValueException"))
		            {
				        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
						$app->response->setStatus ( $result['responseCode']);
    					$app->response->setStatus(400);	
				    } else {
    					$result = $result_obj->internalErrorException ($e);
    					$app->response->setStatus($result['responseCode']);		
				    }			
				}
				echo json_encode($result, JSON_UNESCAPED_UNICODE);
		    }
		);
		
		
		
		
		/*Метод MedicalInfo */
		$app->post('/MedicalInfo',
		    function () use ($app, $result_obj) {
		        try {
		            $token = $app->request->headers->get('Token'); 
		            $post = $result_obj->postJson();
		            
                    $obj = new Auth\ContextDataInfo($post);
                       
                    if (json_last_error() != 0) {
                        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidRequest'], 400);
						$app->response->setStatus ( $result['responseCode']);
                        echo json_encode($result, JSON_UNESCAPED_UNICODE);
                        exit;
                    } 
		            
		            if (!empty($token) && !empty($post)) {
		                $result = $obj->editMedicalInfo($token);
		                $app->response->setStatus ($result['responseCode']);
		            } else {
		                $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidParams'], 400);
						$app->response->setStatus ( $result['responseCode']);
		            }
					$app->response->setStatus ($result['responseCode']);	
		              
		      } catch  (Exception $e)  {
		             if (is_a($e, "Firebase\JWT\SignatureInvalidException")
		                 or is_a ($e, "UnexpectedValueException")
		                 or is_a ($e, 'DomainException')
		             )
		             {
				        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
						$app->response->setStatus ( $result['responseCode']);
    					$app->response->setStatus(400);	
				    } elseif (is_a ($e, "ErrorException")) {
				        
				        /*
				        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidRequest'], 400);
						$app->response->setStatus ( $result['responseCode']);
    					$app->response->setStatus(400);	
    					*/
    					$result = $result_obj->internalErrorException ($e);
    					$app->response->setStatus($result['responseCode']);	
    					
				    } else {
    					$result = $result_obj->internalErrorException ($e);
    					$app->response->setStatus($result['responseCode']);		
				    }
		      }

		   
		      echo json_encode($result, JSON_UNESCAPED_UNICODE);
		    }
		    
		    
		);
		
		
		
		//Метод VedCard
		$app->get('/MedCard', 
		    function () use ($app, $result_obj) {
		         try {
		             $token = $app->request->headers->get('Token'); 
		             $obj = new Auth\User(); 
		             
		             if (!empty($token)) {
		                $result = $obj->getMedCard($token);
		                $app->response->setStatus ($result['responseCode']);
		            } else {
		                $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
						$app->response->setStatus ( $result['responseCode']);
		            }
					$app->response->setStatus ($result['responseCode']);	
		             
		         } catch (Exception $e) {
		             
		               
		             if (is_a($e, "Firebase\JWT\SignatureInvalidException")
		                 or is_a ($e, "UnexpectedValueException")
		                 or is_a ($e, 'DomainException')
		                // or is_a ($e, 'ErrorException')
		             ) {
				        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
						$app->response->setStatus ( $result['responseCode']);
    					$app->response->setStatus(400);	
				    } else {
    					$result = $result_obj->internalErrorException ($e);
    					$app->response->setStatus($result['responseCode']);		
				    }
		         }
		         echo json_encode($result, JSON_UNESCAPED_UNICODE);
		    }
		);
		
		
		
		//Метод Удалить профиль
		$app->post('/delProfile', 
		    function () use ($app, $result_obj) {
		         try {
		             $token = $app->request->headers->get('Token'); 
		             $obj = new Auth\Auth(); 
		             
		             if (!empty($token)) {
		                $result = $obj->delProfile($token);
		                $app->response->setStatus ($result['responseCode']);
		            } else {
		                $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
						$app->response->setStatus ( $result['responseCode']);
		            }
					$app->response->setStatus ($result['responseCode']);	
		             
		         } catch (Exception $e) {
		             if (is_a($e, "Firebase\JWT\SignatureInvalidException")
		                 or is_a ($e, 'DomainException')
		             ) {
				        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
						$app->response->setStatus ( $result['responseCode']);
    					$app->response->setStatus(400);	
				    } else {
    					$result = $result_obj->internalErrorException ($e);
    					$app->response->setStatus($result['responseCode']);		
				    }
		         }
		         echo json_encode($result, JSON_UNESCAPED_UNICODE);
		    }
		);
		
		
		//Метод Logout
		$app->post('/Logout', 
		    function () use ($app, $result_obj) {
		         try {
		             $token = $app->request->headers->get('Token'); 
		             $obj = new Auth\Auth(); 
		             
		             if (!empty($token)) {
		                $result = $obj->userLogout($token);
		                $app->response->setStatus ($result['responseCode']);
		            } else {
		                $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
						$app->response->setStatus ( $result['responseCode']);
		            }
					$app->response->setStatus ($result['responseCode']);	
		             
		         } catch (Exception $e) {
		             
		             if ( is_a($e, "Firebase\JWT\SignatureInvalidException")
		                 or is_a($e, "UnexpectedValueException")
		             ) {
				        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
						$app->response->setStatus ( $result['responseCode']);
    					$app->response->setStatus(400);	
				    } else {
    					$result = $result_obj->internalErrorException ($e);
    					$app->response->setStatus($result['responseCode']);		
				    }
		         }
		         echo json_encode($result, JSON_UNESCAPED_UNICODE);
		    }
		);
		
		
	}	
);


/*Группа для роутинга публичного контента*/
$app->group('/public', 
	function () use ($app, $result_obj) {
	    
	    /*Метод Services*/
		$app->get('/Services',
		    function () use ($app, $result_obj) {
		        try {	
		            $get = $app->request->get();
		            $obj = new Auth\User ();
		            $result = $obj->getServices ($get);
		            $app->response->setStatus ($result['responseCode']);
				} catch (Exception $e) {
    				$result = $result_obj->internalErrorException ($e);
    				$app->response->setStatus($result['responseCode']);		
				}
				echo json_encode($result, JSON_UNESCAPED_UNICODE);
		    }
		);
		
		/*Метод Clinics*/
		$app->get('/Clinics',
		    function () use ($app, $result_obj) {
		        try {
		            $get = $app->request->get();
		            $obj = new Auth\User (); 
		            $result = $obj->getClinics($get);
		            $app->response->setStatus ($result['responseCode']);
				} catch (Exception $e) {
    				$result = $result_obj->internalErrorException ($e);
    				$app->response->setStatus($result['responseCode']);		
				}
				echo json_encode($result, JSON_UNESCAPED_UNICODE);
		    }
		);
		
		
	    /*Метод Call me*/
		$app->post('/CallMe',
		    function () use ($app, $result_obj) {
		        try {	
		            $post = $result_obj->postJson();
		            
                    $obj = new Auth\User(); 
                    
                    if (json_last_error() != 0) {
                        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidRequest'], 400);
						$app->response->setStatus ( $result['responseCode']);
                        echo json_encode($result, JSON_UNESCAPED_UNICODE);
                        exit;
                    } 
                    
                    //Проверить и смерджить настройки почты
          
                    $phone = phoneValidate($post['phone']);
		            if (!empty($phone) && empty($post['callMe'])) {
		                
		                echo $phone;
		                exit;
		                //$phone = $post['phone'];
		                $result = $obj->callMe($phone);
		                $app->response->setStatus ($result['responseCode']);
		            } else if (!empty($phone) 
		                        && !empty(phoneValidate($post['callMe']))
		            ) {
		                $callMe = phoneValidate($post['callMe']);
		                
		                $result = $obj->callMe($phone, $callMe);
		                $app->response->setStatus ($result['responseCode']);
		            } else {
		                $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidParams'], 400);
						$app->response->setStatus ( $result['responseCode']);
		            }
					$app->response->setStatus ($result['responseCode']);	
				} catch (Exception $e) {
					if (is_a($e, "Firebase\JWT\SignatureInvalidException") 
				        or is_a($e, "UnexpectedValueException")
				    ) 
				    {
				        $result = $result_obj->resultErrorHTTP (400, $obj->contentTextArr['invalidToken'], 400);
						$app->response->setStatus ( $result['responseCode']);
    					$app->response->setStatus(400);	
				    } else {
    					$result = $result_obj->internalErrorException ($e);
    					$app->response->setStatus($result['responseCode']);		
				    }				
				}
				echo json_encode($result, JSON_UNESCAPED_UNICODE);
		    }
		);
		
		
		
		
	    
	    

		/*Метод Contacts*/
		$app->post('/ContactUs',
		    function () use ($app, $result_obj) {
		        try {	
		            
		            echo "Contact Us";
		            exit;

					$app->response->setStatus ($result['responseCode']);	
				} catch (Exception $e) {
					$result = $result_obj->internalErrorException ($e);
					$app->response->setStatus($result['responseCode']);					
				}
				echo json_encode($result);
		    }
		);
		
		
		/*Метод AboutUs*/
		$app->post('/About',
		    function () use ($app, $result_obj) {
		        try {	
		            
		            echo "About";
		            exit;

					$app->response->setStatus ($result['responseCode']);	
				} catch (Exception $e) {
					$result = $result_obj->internalErrorException ($e);
					$app->response->setStatus($result['responseCode']);					
				}
				echo json_encode($result);
		    }
		);
	}
);








// GET route
$app->get( '/test/', 
	function () use ($app, $result_obj) {
	    
	    try {
		    
		    $obj = new Auth\Auth ();
			
    	    $result['responseCode'] = 200;
    		$result['response']['msg'] = 'База подключена корректно';
    		$result['result'] = true;
			$app->response->setStatus ( 200);
			
			
		} catch (Exception $e) {
			$result = $result_obj->internalErrorException ($e);
			$app->response->setStatus ( $result['responseCode'] );					
		}
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
);



$app->get('/qqqq/', 

	function () use ($app) {
        //$app->response->headers->set('Content-Type', 'text/plain');
        echo '{"test":"test"}';
	}

);





// POST route
$app->post(
    '/post',
    function () {
        echo 'This is a POST route';
    }
);

// PUT route
$app->put(
    '/put',
    function () {
        echo 'This is a PUT route';
    }
);

// PATCH route
$app->patch('/patch', function () {
    echo 'This is a PATCH route';
});

// DELETE route
$app->delete(
    '/delete',
    function () {
        echo 'This is a DELETE route';
    }
);

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
//$app->response->headers->set('Content-Type', 'application/json');
$app->run();




