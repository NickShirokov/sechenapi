<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitc8b80a90be4223aeee756fc1790ee289
{
    public static $prefixLengthsPsr4 = array (
        'F' => 
        array (
            'Firebase\\JWT\\' => 13,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Firebase\\JWT\\' => 
        array (
            0 => __DIR__ . '/..' . '/firebase/php-jwt/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitc8b80a90be4223aeee756fc1790ee289::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitc8b80a90be4223aeee756fc1790ee289::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
