<?php 
//Валидация номера телефона

function phoneValidate($number, $format = '[1][(3)]3-2-2'){
    
    
    if (preg_match('/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', $number)) {
        
        $plus = ($number[0] == '+'); // есть ли +
        $number = preg_replace('/\D/', '', $number); // убираем все знаки кроме цифр
     
        $len = array_sum(preg_split('/\D/', $format)); // получаем сумму чисел из $format
        $params = array_reverse(str_split($number)); // разбиваем $number на цифры и переворачиваем массив
        $params += array_fill(0, $len, 0); // забиваем пустаты предыдущего массива нулями
     
        $format = strrev(preg_replace('/(\d)/e', "str_repeat('d%', '\\1')", $format)); // делаем форматированную строку и переворачиваем её
        $format = call_user_func_array('sprintf', array_merge(array($format), $params)); // заполняем строку цирами
        $format = ($plus ? '+' : '').strrev($format); // возвращаем строку в нормальное положение и прилепляем + обратно, если он был
     
        if (preg_match_all('/\[(.*?)\]/', $format, $match)) // тут чистим от необязательных кусков
            for ($i = 0, $c = count($match[0]); $i < $c; $i++)
                if (!(int)preg_replace('/\D/', '', $match[1][$i]))
                    $format = str_replace($match[0][$i], '', $format);
     
        $result = strtr(trim($format), array('[' => '', ']' => ''));
        
        //Заменяем символ
        if ($result[0] == 8) {
            $result = substr_replace($result, '+7', 0, 1);
        }elseif ($result[0] == 7) {
            $result = substr_replace($result, '+7', 0, 1);
        }
        
        return $result;
    
    } else {
        return false;
    }
}



?>