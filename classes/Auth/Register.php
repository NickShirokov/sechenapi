<?php 
/*Класс дл¤ работы с регистрацией*/
namespace Auth;



use DataBases as DataBases;
use \Firebase\JWT\JWT;

class Register extends Base {
	
	public $dbh;
	
	public $contentTextArr;
	public function __construct () {
		$this->dbh = DataBases\DB_users::connect();	
		$this->contentTextArr = $this->getTextTemplates();
	}
	
	/*Метод для регистрации пользователя*/
	public function registerUser ($post) {	
	    
	    $sex_arr = $this->sex_arr;
		$dbh = $this->dbh;
		
		/*Пока проверка на непустые поля*/
		if (!empty($post['password']) 
		    && $this->checkPassLength($post['password'])
			&& !empty($post['birthDate'])
			&& strtotime($post['birthDate']) !== FALSE
			&& !empty($post['firstName'])
			&& !empty($post['lastName'])
			&& !empty($post['middleName'])
		    && in_array($post['sex'], $sex_arr)
		) 
		{
		    $email = $post['email'];
		    $token = $this->getToken ($email);
			$pass = $post['password'];
			$pass = md5(md5(trim($post['password'])));
			$birthDate = $post['birthDate'];
			$firstName = $post['firstName'];
			$lastName = $post['lastName'];
			$middleName = $post['middleName'];
			$sex = $post['sex'];
			
			$result = $dbh->insertRegisterUser( $email, 
			                                    $pass, 
			                                    $firstName, 
			                                    $lastName, 
			                                    $middleName, 
			                                    $birthDate,
			                                    $sex,
			                                    $token
			                                    );
			$res['responseCode'] = 200;	
			
			
			if ($result['result']) {
			    $res['result'] = true;
				$res['response']['token'] = $token;
			    $tpl_msg = $this->contentTextArr['emailRegister'];
			    $link = 'http://'.$_SERVER['SERVER_NAME'].'/api/user/verify?token='.$token;
			 
			    $message = '';
			   
			    $fullName = $lastName.' '.$firstName.' '.$middleName;
			    $message.= $this->insertTemplateVarByIndex($tpl_msg, $link, 'link');
			    $message = $this->insertTemplateVarByIndex($message, $fullName, 'fullName');
			    $subject =  $this->contentTextArr['registerSubject'];
			    $this->sendMailForm ($email, $subject, $message);
			} else {
			    $res['result'] = false;
				$msg = $this->contentTextArr['regUserExist'];
				$res['error']['msg'] = $msg;
			}
		} else {
		    $res['result'] = false;
			$res['responseCode'] = 400;
			$res['error']['msg'] = $this->contentTextArr['invalidParams'];
		}
		return $res;
	}
	
	
	/*Меторд для подтверждения регистрации из email*/
	public function checkRegisteredEmail($token) {
	    
	    $dbh = $this->dbh;
	    $email = $this->decodeToken($token)->email;
	    $result = $dbh->checkRegisteredEmail($email, $token);
        
        if ($result['errorCode'] == 0) {
            $res = $this->contentTextArr['verifyEmail'];
        } elseif ($result['errorCode'] ==1) {
            $res = $this->contentTextArr['invalidLink'];
        } elseif ($result['errorCode'] == 2) {
            $res = $this->contentTextArr['notRegister'];
        }
	    return $res;
	}
	
	/*Метод повторной активации*/
	public function reActivate ($token) {
	    
	    $dbh = $this->dbh;
	    $email = $this->decodeToken($token)->email;
	    $resik = $dbh->checkToken($email, $token);

	    if ($resik['errorCode'] == 0) {
    	    $newToken = $this->getToken($email);
    	    $result = $dbh->reActivate($email, $newToken);
    	    //
    	    if ($result['errorCode'] == 0) {
    	        $res['result'] = true;
    	        $tpl_msg = $this->contentTextArr['reActivate'];
    	        $res['response']['msg'] = $this->insertTemplateVar($tpl_msg, $email);
    	        $res['response']['newToken'] = $newToken;
    	        $res['responseCode'] = 200;
    	        
    	        
    	        $message = '';
    	        
    	        $lastName = $result['lastName'];
    	        $firstName = $result['firstName'];
    	        $middleName = $result['middleName'];
    	        
    	        $fullName = $lastName.' '.$firstName.' '.$middleName;
    	        
    	        $tpl_msg_email = $this->contentTextArr['emailRegister'];
			    $link = 'http://'.$_SERVER['SERVER_NAME'].'/api/user/verify?token='.$newToken;
			   
			    //$message = $this->insertTemplateVar($tpl_msg_email, $link);
			   
			    $message.= $this->insertTemplateVarByIndex($tpl_msg_email, $link, 'link');
			    $message = $this->insertTemplateVarByIndex($message, $fullName, 'fullName');
			   
			    $subject =  $this->contentTextArr['reActivateSubject'];
			    $this->sendMailForm ($email, $subject, $message);
    	        
    	    } elseif ($result['errorCode'] == 1) {
    	        $res['result'] = false;
    	        $tpl_msg = $this->contentTextArr['reActivateTime'];
    	        $time = date("i:s",360 - $result['time']);
    	        $res['response']['msg'] = $this->insertTemplateVar($tpl_msg, $time);
    	        $res['timer'] = $time; 
    	        $res['responseCode'] = 200;
    	        
    	    } elseif ($result['errorCode'] == 2) {
    	        $res['result'] = false;
    	        $res['response']['msg'] = $this->contentTextArr['notRegister'];
    	        $res['responseCode'] = 200;
    	    }
      
	    } else {
	        $res['result'] = false;
    	    $res['error']['msg'] =  $this->contentTextArr['invalidToken'];
    	    $res['responseCode'] = 400;
	    }
        return $res;
	}
	
}

?>