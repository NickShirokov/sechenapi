<?php 
//Класс для работы MedicalInfo для обычной передачи JSON запроса

namespace Auth;

use DataBases as DataBases;

class SechenDataInfo extends Base 
{
        public $appType = 'Sechen';
        
        //Метод для добавления Мед Данных
        public function editMedicalInfo($token, $post) {
            
            $dbh = $this->dbh;
    	    $email = $this->decodeToken($token)->email;
    	    $result = $dbh->checkToken($email, $token);
    	    
    	    //Проверяем наличие appType
    	    if (isset($post['appType']) 
    	        && $post['appType'] == $this->appType
    	        && !empty($post['data'])
    	    ) { 
        	    //ID пользователя
        	    $userIdRes = $dbh->select_to_array ("SELECT id FROM users WHERE email='$email' AND deleted='0'");
        	    $user_id = $userIdRes[0];
        	    if (isset($result['errorCode']) && $result['errorCode'] == 0) {
                   //Костыль для birthDate
                    $post = $post['data'];
                   
                    if (isset($post['birthDate']) && strtotime($post['birthDate']) !== FALSE){
                       $dbh->editUserBirth($email, $post['birthDate']);
                       unset($post['birthDate']);
                    } 
                   //Обновляем пользователя
                   $sex_arr = $this->sex_arr;
                   if (!empty($post['firstName'])) {
                       $dbh->editUserFirstName( $email, $post['firstName']);
                       unset($post['firstName']);
                   }
                   
                    if (!empty($post['lastName'])) {
                       $dbh->editUserLastName( $email, $post['lastName']);
                       unset($post['lastName']);
                   }
                   if (!empty($post['middleName'])) {
                       $dbh->editUserMiddleName( $email, $post['middleName']);
                       unset($post['middleName']);
                   }
                   
                    if (!empty($post['sex'])
                        && in_array($post['sex'], $sex_arr)
                    )
                    {
                       $dbh->editUserSex( $email, $post['sex']);
                       unset($post['sex']);
                   }
                   
                   if (!empty($post)) {
                        $test_arr = array();
                        $res['response']['outResults']['result'] = false;
                        
                        //Новый алгоритм поста для медданных в базу
                        foreach ( $post as $key => $value ) { 
                            
                            $var_val = $value;
                            $type = $key;
                            $result = $dbh->insertMedicalParams($type);
            
                            $result = $result[0];
            
                            if (!is_array($var_val)) {
                                $test_arr[] = '("'.$user_id.'", "'.$result['mTypeId'].'", "'.$value.'")';
                                
                                //Референсные значения
                               if (!empty($result['minVal'])
                                   && !empty($result['maxVal'])
                               ) 
                               {
                                   //Сравниваем параметры для float
                                    $val_par = str_replace(',','.',$value);
                                   
                                    $var_par = (float)$val_par;
                                    $min = (float)$result['minVal'];
                                    $max = (float)$result['maxVal'];
                                    
                                    if ( $var_par > $max 
                                        or $var_par < $min 
                                    ) 
                                    {
                                          $res['response']['outResults']['result'] = true; 
                                          $res['response']['outResults']['types'][] = $type;
                                    } else {
                                          $res['response']['outResults']['result'] = false;
                                    }
                               } 
                            } else {
                                foreach ($var_val as $key => $value) {
                                    $test_arr[] = '("'.$user_id.'","'.$result['mTypeId'].'", "'.$value.'")';
                                }
                            }
                        }
            
                        $str_params = implode (',',$test_arr);
                        $lastInsId = $dbh->insertMedicalParamsNew($str_params);
                        $date = $dbh->select_to_array ("SELECT dateEdit FROM medical_cards WHERE id='$lastInsId'");
                        $date = $date[0];
                        $res['response']['dataDate'] = $date;
                   } 
                   
                   $res['result'] = true;
        	       $res['response']['msg'] = "Ваша мед карта успешно изменена";
        	       $res['responseCode'] = 200;
        	   } else {
        	       $res['result'] = false;
        	       $res['error']['msg'] = $this->contentTextArr['invalidToken'];
        	       $res['responseCode'] = 400;
        	   }
    	    } else {
    	        $res['result'] = false;
        	    $res['error']['msg'] = $this->contentTextArr['invalidParams'];
        	    $res['responseCode'] = 400;
    	    }
            return $res;
        }
    
    
}


?>