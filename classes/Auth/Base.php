<?php 
/*Класс дл¤ работы с регистрацией*/
namespace Auth;

use DataBases as DataBases;
use \Firebase\JWT\JWT;


class Base {
	
	public $dbh;
	private $key = 'my_key';
	
	public $contentTextArr;
	
	public $sex_arr = array(
                       '0' => 'male',
                       '1' => 'female'
                       );
	
	public function __construct () {
		$this->dbh = DataBases\DB_users::connect();	
	    $this->contentTextArr = $this->getTextTemplates();
	}
	
	
	
	/*Метод создает и возвращает токен*/
	public function getToken ($email) {
	    
	    //JWT::$leeway = 60;
        $payload = array(
                    "email" => $email,
                    "ee" => time()
                    //"exp" => time()
        	); 
        $token = JWT::encode($payload, $this->key);
	    return $token;    
	}
	
	public function decodeToken ($token) {
        $arr = JWT::decode($token, $this->key, array('HS256'));
        return $arr;
	}
	
	
	/*Проверка email*/
	public function checkEmail ($email) {
	    
	    $dbh = $this->dbh;
	    $result = $dbh->checkEmail ($email);
		if ($result['errorCode'] == 0) {
		    $res['result'] = true;
		    $res['response']['msg'] = 'Данный email используется';
		} elseif($result['errorCode'] == 1) {
		    $res['result'] = false;
			$res['error']['msg'] = 'Такой email не зарегистрирован';
		}
		$res['responseCode'] = 200;
		return $res;
	}
	/*Проверка пароля*/
	public function checkPassLength ($password) {
    	$res = true;	
    	if ( strlen($password) < 8 or strlen($password) > 64 ) {
    		$res = false;
    	}
    	return $res;
    }
    
    
    
    /*Функция генерации пароля*/
    public function generatePassword($number) {
        $arr = array('a','b','c','d','e','f',
                     'g','h','i','j','k','l',
                     'm','n','o','p','r','s',
                     't','u','v','x','y','z',
                     'A','B','C','D','E','F',
                     'G','H','I','J','K','L',
                     'M','N','O','P','R','S',
                     'T','U','V','X','Y','Z',
                     '1','2','3','4','5','6',
                     '7','8','9','0','(',')',
                     '[',']','!','?','&','^',
                     '%','@','*','$','/','|',
                    );
        // Генерируем пароль
        $pass = "";
        for($i = 0; $i < $number; $i++)
        {
          // Вычисляем случайный индекс массива
          $index = rand(0, count($arr) - 1);
          $pass.= $arr[$index];
        }
        return $pass;
    }
    
    
    /* Обертка для отправки почты */
    public function sendMailForm ($to, $subject, $message) {
    	
    	date_default_timezone_set('Etc/UTC');
    	$mail = new \PHPMailer;
        //$mail->SMTPDebug = 3;                               // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = SMTP_HOST;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = SMTP_USER;                 // SMTP username
        $mail->Password = SMTP_PASS;                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = SMTP_PORT;   
        $mail->CharSet  = 'UTF-8';
        // TCP port to connect to
        
        $mail->setFrom(MAIL_FROM, NAME_FROM);
        $mail->addAddress($to);     // Add a recipient               // Name is optional
        $mail->addReplyTo( MAIL_REPLY, $subject);
        //$mail->addCC(MAIL_REPLY);
        //$mail->addBCC('bcc@example.com');
        
        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML
        
        $mail->Subject = $subject;
        $mail->Body    = $message;
        $mail->AltBody = $message;
        
        $mail->send();
        /*if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }
        /*

    	exit;
    	
    	
    	$headers = 'From: noreply@lionsdigital.pro' . "\r\n" .
    					'Reply-To: noreply@lionsdigital.pro' . "\r\n" .
    					'X-Mailer: PHP/' . phpversion() . "\r\n";
    	$headers.= 'Cc: noreply@lionsdigital.pro' . "\r\n";
    	$headers.= 'MIME-Version: 1.0' . "\r\n";
        $headers.= 'Content-type: text/html; charset=utf-8' . "\r\n";
    	mail($to, $subject, $message, $headers);
        */

    } 
	
	
	/*Метод получает из базы список текстовых шаблонов*/
	public function getTextTemplates () {
	    $dbh = $this->dbh;
	    $result = $dbh->getTextTemplates();
	    return $result;
	} 
	
	
	/*Метод подставляет в шаблон контента переменную*/
	public function insertTemplateVar ($text,  $value) {
	    $reg = "/{{[a-zA-Z]*}}/";
	    //$str 
	    $res = preg_replace ($reg, $value, $text);
        return $res;
	}
	
	
	/*Метод подставляет в шаблон контента переменную по её индексы у базе*/
	public function insertTemplateVarByIndex ($text,  $value, $index) {
	    $reg = "/{{".$index."}}/";
	    //$str 
	    $res = preg_replace ($reg, $value, $text);
        return $res;
	}
	


}

?>