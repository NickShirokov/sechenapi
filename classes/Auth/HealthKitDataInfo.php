<?php 
//Класс для работы MedicalInfo для передачи JSON запроса из HealthKIt

namespace Auth;

use DataBases as DataBases;

class HealthKitDataInfo extends Base 
{
    
    public $appType = 'HealthKit';    
    
    //Метод для добавления Мед Данных
    public function editMedicalInfo($token, $post) { 
        
        $dbh = $this->dbh;
    	$email = $this->decodeToken($token)->email;
    	$result = $dbh->checkToken($email, $token);
    	
        
        //Проверяем наличие appType
    	if (isset($post['appType']) 
    	    && $post['appType'] == $this->appType
    	    && !empty($post['data'])
    	) 
    	{ 
    	   
    	    if (isset($result['errorCode']) && $result['errorCode'] == 0) { 
    	        
    	         //ID пользователя
        	    $userIdRes = $dbh->select_to_array ("SELECT id FROM users WHERE email='$email' AND deleted='0'");
        	    $user_id = $userIdRes[0];
    	        
    	        $data = $post['data'];
    	        $test_arr = array();
    	       
    	        //Проходим циклом по полученным данным
    	        foreach ($data as $key => $value) {
    	            
    	            $samples = $value['samples'];
    	            $date = $value['date'];
    	            $date = date ("Y-m-d H:i:s", strtotime($date));
    	            
    	            //Проходим циклом по семплам
    	            if (!empty($samples)) {
    	                foreach ($samples as $key => $value) {
    	                    $type = $key;
    	                    $result = $dbh->insertMedicalParamAlias($type, $this->appType);
    	                    
                            $result = $result[0];
                            //Ghоверяем на пустое, чтобы не писать в БД
                            if (!empty($value) or is_numeric($value)) {
    	                        $test_arr[] = '("'.$user_id.'", "'.$result['mTypeId'].'", "'.$value.'", "'.$date.'", "'.$this->appType.'")';
                            }
    	                }
    	            }
    	            
    	        }
    	        
    	        $str_params = implode (',',$test_arr);
                $lastInsId = $dbh->insertMedicalParamsWithDate($str_params);
    	        $res['result'] = true;
        	    $res['response']['msg'] = "Ваша мед карта успешно изменена";
        	    $res['responseCode'] = 200;
    	        
    	        
    	    } else {
    	       $res['result'] = false;
    	       $res['error']['msg'] = $this->contentTextArr['invalidToken'];
    	       $res['responseCode'] = 400;
        	}
    	} else {
    	    $res['result'] = false;
        	$res['error']['msg'] = $this->contentTextArr['invalidParams'];
        	$res['responseCode'] = 400;
    	}
        return $res;
    }
    
}

?>