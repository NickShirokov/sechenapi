<?php 
/*Класс для работы с пользователем*/
namespace Auth;

use DataBases as DataBases;


class User extends Base {
	
	public $dbh;
	public function __construct () {
		$this->dbh = DataBases\DB_users::connect();	
		$this->contentTextArr = $this->getTextTemplates();
	}
	
	/*Метод получает сервисы - пока в этом классе, далее перенесем, если нужно*/
	public function getServices ($get) {
	    
	    $dbh = $this->dbh;
	    //Dывод по конкретной услуге по serviceId
	    if (isset($get['serviceId']) 
	        && is_numeric($get['serviceId'])
	    ) 
	    {
	        $s_id = $get['serviceId'];
	        $result = $dbh->getServiceByServiceId ($s_id);
	        if (!empty($result) && isset($result[0])) {
	            $result = $result[0];
	        }
	        
	        //Slider
	        if (!empty($result['slider'])) {
	            $result['slider'] = explode(", ", $result['slider']);
	        } else {
	            unset($result['slider']);
	        }
	        //PArent
	        if(empty($result['parent'])) {
	            unset($result['parent']);
	        }
    
	    } elseif (isset($get['clinicId']) 
	        && is_numeric($get['clinicId'])
	   ) 
	   {
	        $cl_id = $get['clinicId'];
	        $result = $dbh->getServiceByClinicId ($cl_id);
            foreach ($result as $key => $value) {
                if (empty($result[$key]['parent'])) {
                    unset($result[$key]['parent']);
                }
            }
	    } elseif (isset($get['parentId'])
	            && is_numeric($get['parentId'])
	    ) 
	    {
	        $p_id = $get['parentId'];
	        $result = $dbh->getServiceByParentId ($p_id);
	    } elseif (empty($get['serviceId']) 
	            && empty($get['clinicId']) 
	            && empty($get['parentId']) 
	            && empty($get['searchQuery']) 
	    ) 
	    {
	        $result = $dbh->getServiceByClinicId ('');
            foreach ($result as $key => $value) {
                if (!$value['parent']) {
                    unset($result[$key]['parent']);
                }
            }
	    }

        //Обработка вывода
	    if (!empty($result)) {
	        $res['result'] = true;
	        $res['response'] = $result;
	    } else {
	        $res['result'] = true;
	        $res['response']['msg'] = 'По запросу ничего не найдено';
	    }
	    $res['responseCode'] = 200;
        return $res;
	}
	
	
	/*Метод получает сервисы - пока в этом классе, далее перенесем, если нужно*/
	public function getClinics ($get) {
	    
	    $dbh = $this->dbh;
	    
	    if (isset($get['clinicId']) 
	        && $get['clinicId'] != ''
	        && is_numeric($get['clinicId'])
	    ) 
	    {
	        $cl_id = $get['clinicId'];
	        $result = $dbh->getClinicsByClinicId ($cl_id);
	        
	        if (!empty($result) && isset($result[0])) {
	            $result = $result[0];
	        }
	        
	         //Slider
	        if (!empty($result['slider'])) {
	            $result['slider'] = explode(", ", $result['slider']);
	        } else {
	            unset($result['slider']);
	        }
	        
	         //Сервисы
	        if (isset($result['services']) && !empty($result['services'])) {
	            $result['services'] = explode(", ", $result['services']);
	        } else {
	            unset($result['services']);
	        }
	        
	        //Emфшд
			$emails = $dbh->select_fetch_assoc ("SELECT typeEmail AS name, valueEmail AS email
				                                FROM emails WHERE clinicId = '$cl_id'");
			if (!empty($emails)) {
			    $result['emails'] = $emails;	                                
			}
			//Phones
			$phones = $dbh->select_fetch_assoc ("SELECT typeNumber AS name, phoneNumber AS number
				                                FROM phones WHERE clinicId = '$cl_id'");
			if (!empty($phones)) {
			    $result['phones'] = $phones;	                                
			}
			//Map
			if (!empty($result['mapX']) && !empty($result['mapY'])) {
			    $result['map']['n'] = $result['mapX'];
			    $result['map']['e'] = $result['mapY'];
			    
			}
			
			unset($result['mapX']);
			unset($result['mapY']);
	    } elseif (isset($get['serviceId'])
	            && is_numeric($get['serviceId'])
	    ) 
	    {
	        $s_id = $get['serviceId'];
	        $result = $dbh->getClinicsByServiceId ($s_id);
	    } else {
	        $result = $dbh->getClinicsByClinicId ('');
	        
	        foreach ($result as $key => $value) {
    	        if (!empty($result[$key]['services'])) {
    	            $result[$key]['services'] = explode(", ", $result[$key]['services']);
    	        } else {
    	            unset($result[$key]['services']);
    	        }
	        }
	    }
	    
	    if(isset($result[1]['message']) ) {
	        $res['result'] = false;
	        $res['error']['msg'] = '';
	        $res['error']['msg'] = $this->contentTextArr['notRegister'];
	    } elseif (isset($result[2]['message']) ) {
	        $res['result'] = false;
	        $res['error']['msg'] = $this->contentTextArr['invalidToken'];
	    } elseif (!empty($result)) {
	        //Слайдер
	        if (isset($result[0]['slider']) && !empty($result[0]['slider'])) {
	            $result[0]['slider'] = explode(", ", $result[0]['slider']);
	        } else {
	            unset($result[0]['slider']);
	        }
	        $res['result'] = true;
	        $res['response'] = $result;
	    } else {
	        $res['result'] = true;
	        $res['response']['msg'] = 'По запросу ничего не найдено';
	    }
	    $res['responseCode'] = 200;
        return $res;

	}
	
		/*Метод реализует звонок пользователю на указанный телефон*/
	public function callMe ($phone, $callMe = '') {
	    
	    $dbh = $this->dbh;
	    
	    $email = WORK_OUR_EMAIL;

		if (!empty($callMe)) {
			$email = $callMe;
		} elseif (!empty($_GET['autoTests']) && $_GET['autoTests'] == 'true') {
		    $email = TEST_OUR_EMAIL;
		}
        $subject = $this->insertTemplateVar ($this->contentTextArr['callSubject'],  $phone);
        $message = $this->insertTemplateVar ($this->contentTextArr['callBodyMessage'],  $phone); 
        $res['response']['msg'] = $this->contentTextArr['callMsg'];
	    $res['result'] = true;     
        $dbh->callMe($phone);
        $this->sendMailForm ($email, $subject, $message);
	    
	    $res['responseCode'] = 200;
        return $res;
	}
	
	
	/*Vетод получения медкарты пользователя*/
	public function getMedicalInfo ($token) {
	    
	    $dbh = $this->dbh;
	    $email = $this->decodeToken($token)->email;
	    $resik = $dbh->checkToken($email, $token);
	    
	    if ($resik['errorCode'] == 0 ) {
	    
    	    $date_arr = $dbh->getDatesMedCard($email);
    	    $new_arr = array();
    	    $arr = $dbh->getMedicalInfo ($email);
    	    
            //Wbrkом идем по всему списку
    	    if (!empty($date_arr) && !empty($arr)) {
        	    foreach ( $arr as $key => $value ) {
        	       
        	       $val_arr = $value;
        	       
        	       $short = $val_arr['shortName'];
        	       
        	       if (in_array($val_arr['dateEdit'], $date_arr)) {
        	           
        	           $date= $val_arr['dateEdit'];
        	           $new_arr[$date]['date'] =  $val_arr['dateEdit'];  
        	           
        	           if (!isset($new_arr[$date]['data'][$short])) {
        	               $new_arr[$date]['data'][$short] =  $val_arr['value'];
        	                //Формируем htathtycyst pyfxtybz
        	               if (!empty($val_arr['minVal'])  && !empty($val_arr['maxVal'])) {
        	                   //Сравниваем параметры для float
        	                   
        	                    $val_par = str_replace(',','.',$val_arr['value']);
        	                   
                                $var_par = (float) $val_par;
                                $min = (float)$val_arr['minVal'];
                                $max = (float)$val_arr['maxVal'];
                                
                                if ( $var_par > $max  or $var_par < $min ) 
                                {
                                   $new_arr[$date]['out'][] = $short;
                                } 
        	               }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
        	           } else {
        	               $new_arr[$date]['data'][$short] = $new_arr[$date]['data'][$short].', '. $val_arr['value'];
        	           }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
        	       }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
        	    }
        	    
        	    $new_arr = array_values($new_arr);
    	    } else {
    	        $new_arr = null;
    	    }
    	    //Проходим по датам
    	    if (!empty($new_arr)) {
    	        $res['result'] = true; 
    	        $res['response'] = $new_arr;
    	        $res['responseCode'] = 200;
    	    } else {
    	        $res['result'] = true;
    	        $res['response']['msg'] = 'По запросу ничего не найдено';
    	        $res['responseCode'] = 200;
    	    }
    	    
	    } else {
	        $res['result'] = false;
    	    $res['error']['msg'] =  $this->contentTextArr['invalidToken'];
    	    $res['responseCode'] = 400;
	    }
	    
        return $res;
	}
	
	
	/*Метод изменения медкарты пользователя*/
	public function editMedicalInfo ($token, $post) {
	    
	    $dbh = $this->dbh;
	    $email = $this->decodeToken($token)->email;
	    $result = $dbh->checkToken($email, $token);
	    //ID пользователя
	    $userIdRes = $dbh->select_to_array ("SELECT id FROM users WHERE email='$email' AND deleted='0'");
	    $user_id = $userIdRes[0];
	    if (isset($result['errorCode']) && $result['errorCode'] == 0) {
   
           //Костыль для birthDate
            if (isset($post['birthDate']) && strtotime($post['birthDate']) !== FALSE){
               $dbh->editUserBirth($email, $post['birthDate']);
               unset($post['birthDate']);
            } 
           //Обновляем пользователя
           $sex_arr = $this->sex_arr;
           if (!empty($post['firstName'])) {
               $dbh->editUserFirstName( $email, $post['firstName']);
               unset($post['firstName']);
           }
           
            if (!empty($post['lastName'])) {
               $dbh->editUserLastName( $email, $post['lastName']);
               unset($post['lastName']);
           }
           if (!empty($post['middleName'])) {
               $dbh->editUserMiddleName( $email, $post['middleName']);
               unset($post['middleName']);
           }
           
            if (!empty($post['sex'])
                && in_array($post['sex'], $sex_arr)
            )
            {
               $dbh->editUserSex( $email, $post['sex']);
               unset($post['sex']);
           }
           
           if (!empty($post)) {
           
                $test_arr = array();
                $res['response']['outResults']['result'] = false;
                
                //Новый алгоритм поста для медданных в базу
                foreach ( $post as $key => $value ) { 
                    
                    $var_val = $value;
                    $type = $key;
                    $result = $dbh->insertMedicalParams($type);
    
                    $result = $result[0];
    
                    if (!is_array($var_val)) {
                        $test_arr[] = '("'.$user_id.'", "'.$result['mTypeId'].'", "'.$value.'")';
                        
                        //Референсные значения
                       if (!empty($result['minVal'])
                           && !empty($result['maxVal'])
                       ) 
                       {
                           //Сравниваем параметры для float
                            $val_par = str_replace(',','.',$value);
                           
                            $var_par = (float)$val_par;
                            $min = (float)$result['minVal'];
                            $max = (float)$result['maxVal'];
                            
                            if ( $var_par > $max 
                                or $var_par < $min 
                            ) 
                            {
                                  $res['response']['outResults']['result'] = true; 
                                  $res['response']['outResults']['types'][] = $type;
                            } else {
                                  $res['response']['outResults']['result'] = false;
                            }
                       } 
                    } else {
                        foreach ($var_val as $key => $value) {
                            $test_arr[] = '("'.$user_id.'","'.$result['mTypeId'].'", "'.$value.'")';
                        }
                    }
                }
    
                $str_params = implode (',',$test_arr);
                $lastInsId = $dbh->insertMedicalParamsNew($str_params);
                $date = $dbh->select_to_array ("SELECT dateEdit FROM medical_cards WHERE id='$lastInsId'");
                $date = $date[0];
                $res['response']['dataDate'] = $date;
           } 
           
           $res['result'] = true;
	       $res['response']['msg'] = "Ваша мед карта успешно изменена";
	       $res['responseCode'] = 200;
	   } else {
	       $res['result'] = false;
	       $res['error']['msg'] = $this->contentTextArr['invalidToken'];
	       $res['responseCode'] = 400;
	   }
       return $res;
	}
	
	
	//Получение данных медкарты
	public function getMedCard ($token) {
	    
	    $dbh = $this->dbh;
	    $email = $this->decodeToken($token)->email;
	    $result = $dbh->checkToken($email, $token);



	    $types = $dbh->getMedTypes('card');
	    $arr = array();
	    foreach ($types as $key => $value) {
	        $key = $value['shortName'];
	        $arr[$key] = null;
	    }
	    
	    if (isset($result['errorCode']) 
	        && $result['errorCode'] == 0
	        && $result['is_active'] == 1
	    ) 
	    {
	         
	         $result = $dbh->getMedCard($email);

	         
	         //Проверяем на пустое и формируем массив вывода
	         if (!empty($result)) {
                foreach ($result as $key => $value) {
                    $short = $value['shortName'];
                    $arr[$short][] = $value['value'];
                       
                }
  
                foreach ($arr as $key => $value) {
                    if (count($value) == 1) {
                        $arr[$key] = $value[0];
                    }
                }
                    $arr['firstName'] = $result[0]['firstName'];
                    $arr['lastName'] = $result[0]['lastName'];
                    $arr['middleName'] = $result[0]['middleName'];
                    $arr['birthDate'] = $result[0]['birthDate'];
                    $arr['sex'] = $result[0]['sex'];
                
                $res['response'] = $arr;
                $res['result'] = true;
	         } else {
	               $user_data = $dbh->select_fetch_assoc ("SELECT firstName, lastName, middleName, birthDate, sex 
	                                                   FROM users WHERE email = '$email' AND deleted='0';");
	               $arr = array_merge ($user_data[0], $arr);
	               $res['result'] = true;
	               $res['response'] = $arr;
	         }
	         $res['responseCode'] = 200;
	         
	    } elseif (isset($result['errorCode']) 
	        && $result['errorCode'] == 0
	        && $result['is_active'] == 0) 
	   {
	        $res['result'] = true;
	        $res['response']['msg'] = $this->contentTextArr['mustActivate'];
	        $res['responseCode'] = 200;
	    } else {
	         $res['result'] = false;
	         $res['error']['msg'] = $this->contentTextArr['invalidToken'];
	         $res['responseCode'] = 400;
	    }
	    return $res;
	}
	
	
	//Получение мед типов
	public function getMedTypes ($token, $get) {
	    
	    $dbh = $this->dbh;
	    $email = $this->decodeToken($token)->email;
	    $result = $dbh->checkToken($email, $token);
	    
	    if (isset($get['type']) && $get['type'] == 'data') {
	        $result = $dbh->getMedTypes('data');
	    } else {
	        $result = $dbh->getMedTypes('card');
	    }
	    
        $res['response']['types'] = $result;
	    $res['responseCode'] = 200;
	    $res['result'] = true;
	    return $res;
	}
	
	
	
}

?>