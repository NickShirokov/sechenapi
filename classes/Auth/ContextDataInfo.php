<?php 
//Класс-контекст для работы с POST MedicalInfo

namespace Auth;

use DataBases as DataBases;

class ContextDataInfo extends Base {
    
    
    public $filters;
    public $post;
    
    public function __construct ($post = array()) {
        $this->post = $post;
        $this->filters = '';
		
		if (isset($post['appType'])) {
		    $this->filters = $post['appType'];
		} 
    }
    
    //Метод для стратегии 
    public function editMedicalInfo ($token) {
        
        switch ($this->filters) {
			case "Sechen":
				$obj = new SechenDataInfo();
				$res = $obj->editMedicalInfo ($token, $this->post);
				break;
			case "HealthKit":
				$obj = new HealthKitDataInfo();
				$res = $obj->editMedicalInfo ($token, $this->post);
				break;
			default:
			    $obj = new User();
			    $post = $this->post;
			    unset($post['appType']);
			    $res = $obj->editMedicalInfo ($token, $this->post);
			    break;
		}
        
        
        return $res;
        
    } 
    
    
}


?>