<?php 
/*Класс для авторизации*/

namespace Auth;

use DataBases as DataBases;


class Auth extends Base {
	
	public $dbh;
	
	public $contentTextArr;
	public function __construct () {
		$this->dbh = DataBases\DB_users::connect();	
		$this->contentTextArr = $this->getTextTemplates();
	}
	
		
	/*Метод авторизует пользователя*/
	public function authUser ($email, $password) {	
		$dbh = $this->dbh;
		$token = $this->getToken($email);
		$result = $dbh->authUser ($email, $password, $token);

		if ($result['errorCode'] == 0) {
		    $res['result'] = true;
		    $res['response']['token'] = $result['token'];
		    $res['response']['user'] = $result;
		    unset($res['response']['user']['token']);
		    unset($res['response']['user']['errorCode']);
		    unset($res['response']['user']['message']);
		} elseif($result['errorCode'] == 1) {
		    $res['result'] = false;
			$res['error']['msg'] = $this->contentTextArr['invalidPass'];
		} elseif ($result['errorCode'] == 2) {
		    $res['result'] = false;
			$res['error']['msg'] = $this->contentTextArr['notRegister'];
		} else {
		    $res['result'] = false;
		    $res['msg'] = $this->contentTextArr['someProblem'];
		}
		$res['responseCode'] = 200;
		return $res;
	}
	
	/*Метод для восстановления пароля*/
	public function restorePassword ($email) {
		$dbh = $this->dbh;
        $password = $this->generatePassword(10);
        $pass = md5(md5($password)); 
		$result = $dbh->restoreUserPassword ($email, $pass);
		
        $res['responseCode'] = 200;
		if ($result['errorCode'] == 0) {
				    
		    $tpl_msg = $this->contentTextArr['instPassEmail'];
			$res['response']['msg'] = $this->insertTemplateVar($tpl_msg, $email);
		    
		    $link = GLOBAL_HOST_FULL.'/api/redirectApp';
		    
		    $fullName = $result['lastName'].' '.$result['firstName'].' '.$result['middleName'];
		    $tpl_ps = $this->contentTextArr['newPass'];
		    $message = $this->insertTemplateVarByIndex($tpl_ps, $password, 'password');
		    $message = $this->insertTemplateVarByIndex($message, $fullName, 'fullName');
		    
		    $message = $this->insertTemplateVarByIndex($message, $link, 'link');
		    
		    $res['result'] = true;
		    $this->sendMailForm ($email, $this->contentTextArr['restorePass'], $message);
		} else {
		    $res['result'] = false;
			$res['error']['msg'] = $this->contentTextArr['notRegister'];
		}
		return $res;
	}
	
	/*Метод для изменения пароля*/
	public function changePassword ($post, $token) {
	    
	    $dbh = $this->dbh;

	    if (!empty($post['password'])
	       && $this->checkPassLength($post['password'])
	       && !empty($post['oldpassword'])
	       && $this->checkPassLength($post['oldpassword'])
	    ) 
	    {
	        $password = $post['password'];
	        $oldPass = $post['oldpassword'];
	        $email = $this->decodeToken($token)->email;
	        $old_pass = md5(md5(trim($oldPass)));
	        $pass = md5(md5(trim($password)));
	        $result = $dbh->changePassword($email, $pass, $old_pass  ,$token);

	        $res['responseCode'] = 200;
    		if ($result['errorCode'] == 0) {
    			$res['response']['msg'] = $this->contentTextArr['changePass'];
    		    $res['result'] = true;
    		} elseif ($result['errorCode'] == 1) {
    		    $res['result'] = false;
    			$res['error']['msg'] = $this->contentTextArr['notRegister'];
    		} elseif ($result['errorCode'] == 2) {
    		    $res['result'] = false;
    			$res['error']['msg'] = $this->contentTextArr['invalidToken'];
    		} elseif ($result['errorCode'] == 3) {
    		    $res['result'] = false;
    			$res['error']['msg'] = $this->contentTextArr['invalidOldPass'];
    		}
    		
            
	    } else {
	        $res['result'] = false;
			$res['responseCode'] = 400;
			$res['error']['msg'] = $this->contentTextArr['shortPassword'];
	    }
	    return $res;
	}
	
	
	//Удалить профиль пользователя
	public function delProfile ($token) {
	     $dbh = $this->dbh;
	     $email = $this->decodeToken($token)->email;
	     $result = $dbh->delProfile($email, $token);
	     
        if ($result['errorCode'] == 0) {
            
            $tpl_msg = $this->contentTextArr['delProfileEmail'];
		    
		    $link = GLOBAL_HOST_FULL.'/api/redirectApp';
		    
		    $fullName = $result['lastName'].' '.$result['firstName'].' '.$result['middleName'];
		    $message = $this->insertTemplateVarByIndex($tpl_msg, $fullName, 'fullName');
		    
		    $message = $this->insertTemplateVarByIndex($message, $link, 'link');
            

		    $this->sendMailForm ($email, $this->contentTextArr['delProfileSubject'], $message);
    		
    		
    		$res['response']['msg'] = $this->contentTextArr['deleteProfile'];
    	    $res['result'] = true;
    	} elseif ($result['errorCode'] == 2) {
    	    $res['result'] = false;
    		$res['error']['msg'] = $this->contentTextArr['notRegister'];
    	} elseif ($result['errorCode'] == 1) {
    	    $res['result'] = false;
    		$res['error']['msg'] = $this->contentTextArr['invalidToken'];
    	}
    	
    	$res['responseCode'] = 200;
	    return $res;
	    
	}
	
	
	//Logout
	public function userLogout ($token) {
	    
	     $dbh = $this->dbh;
	     $email = $this->decodeToken($token)->email;
	     $result = $dbh->userLogout($email, $token);
         
        if ($result['errorCode'] == 0) {
    		$res['response']['msg'] = $this->contentTextArr['logout'];
    	    $res['result'] = true;
    	} elseif ($result['errorCode'] == 2) {
    	    $res['result'] = false;
    		$res['error']['msg'] = $this->contentTextArr['notRegister'];
    	} elseif ($result['errorCode'] == 1) {
    	    $res['result'] = false;
    		$res['error']['msg'] = $this->contentTextArr['invalidToken'];
    	}
    	
    	$res['responseCode'] = 200;
	    return $res;
	}
	
}



?>