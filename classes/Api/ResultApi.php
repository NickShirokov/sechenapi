<?php 
/*����� ��� ���������� ������ � ���*/

namespace Api;

use \Firebase\JWT\JWT;


final class ResultApi {
	
		
	private static $_singleton;	
	public $success_result = array (
							'result' => 'true',
							'responseCode' => 200
						);	
	
	public $mode = 'test';
	//public $mode = 'production';
	
	private function __construct () {
		
		
	}
	
		
	/*���������� ���������*/
	public function __destruct() {
            self::$_singleton = null;
	}
	
	private function __clone(){
    }
    /**
     * ����������� �������, ������� ����������
     * ��������� ������ ��� ������� ����� ���
     * �������������
     *
     * @return SingletonTest
     */
	 
	 public static function getInstance () {
		  
		// ��������� ������������ ����������
        if (null === self::$_singleton) {
            // ������� ����� ���������
            self::$_singleton = new self();
        }
        // ���������� ��������� ��� ������������ ���������
        return self::$_singleton;
    } 
	/*���������� mode*/
	public function setMode ($mode) {
		$this->mode = $mode;
	}
	
	/*����� ���������� ������ ��� ���������� � ����� ������ HTTP*/
	public function resultErrorHTTP ($responseCode, $msg) {
		
		return array(
					'result' => 'false',
					'error' => array(
					                'msg' => $msg 
					            ),
					 'responseCode' => $responseCode
					);
	}
	
	
	/*����� ��������� ��������� json*/
	public function postJson () {
	    $postData = file_get_contents('php://input');
        $post = json_decode($postData, true);
        return $post;
	}
	
	

	/*��������� 500 ������*/
	public function internalErrorException ($e) {
		
		$mode = $this->mode;
		if ($mode == 'production') {
			$status = "Internal Server Error";
		} else {
			$status = 'ERROR: file:'.$e->getFile().',line '.$e->getLine().'): '.$e->getMessage();
		}
		
		/*����� � ��� ���� ����� �� ������*/
		$today = date("j-M-Y h:i:s");		
		$mess = "[$today UTC]"." ".$status;
		$file = fopen( "../PHP_error.log", "a+");
		fwrite($file, $mess . "\n");
		fclose($file);
		
		$result = $this->resultErrorHTTP (500, $status);	
		return $result;
	}
	
	
	public function checkInputJson () {
	    
	    
	    
	}
	
	
}


?>