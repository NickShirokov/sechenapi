<?php

namespace DataBases;
use \PDO;

class DB_users  {
		
		private static $_singleton;
		private $dbh;
		private function __construct() {
		$this->_connect(DB_users_HOST, 
						DB_users_USER, 
						DB_users_PASSWORD, 
						DB_users
						);
		
		// set error level to warnings
		$this->dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		
		}	 
		public static function connect() {
			
			if(!self::$_singleton) {
				self::$_singleton = new DB_users();
			}
			return self::$_singleton;
		}
	 
		private function _connect($host, $user, $pass, $db) {
			try {
				$this->dbh = new PDO("mysql:host=$host;
										dbname=$db", 
										$user, 
										$pass, 
										array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
										      // PDO::MYSQL_ATTR_INIT_COMMAND => 'SET sql_mode="ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER"' 
										
										
										        )
										//array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET sql_mode="ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER"')
									);
				return true;
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		final public function __destruct() {
            self::$_singleton = null;
		}
		
		
		//Выборка данных в массив
		public function select_to_array ($sql) {
			try {
				//Получаем массив с нашими группами из базы
				$qry = $this->dbh->prepare($sql);
				$qry->execute();
				/* Извлечение всех значений столбца id_groups */
				$arr = $qry->fetchAll(PDO::FETCH_COLUMN, 0);
				return $arr;
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		} 
		
		public function select_key_pair_array ($sql) {
			try {
				//Получаем массив с нашими группами из базы
				$qry = $this->dbh->prepare($sql);
				$qry->execute();
				/* Извлечение всех значений столбца id_groups */
				$arr = $qry->fetchAll(PDO::FETCH_KEY_PAIR);
				return $arr;
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}	

		public function select_fetch_obj($sql) {
			try {
				//Получаем массив с нашими группами из базы
				$qry = $this->dbh->prepare($sql);
				$qry->execute();
				/* Извлечение всех значений столбца id_groups */
				$arr = $qry->fetchAll(PDO::FETCH_OBJ);
				return $arr;
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}	
		
		public function select_fetch_assoc($sql) {
			try {
				//Получаем массив с нашими группами из базы
				$qry = $this->dbh->prepare($sql);
				$qry->execute();
				/* Извлечение всех значений столбца id_groups */
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr;
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}	
		
		/*Метод получает из базы текстовые шаблоны*/
		public function getTextTemplates () {
		    $result = $this->select_key_pair_array("SELECT short_name, template FROM content_texts;");
		    return $result;
		}

		/*Пробуем функцию записи регистрирующегося пользователя в бд*/
		public function insertRegisterUser ($email, 
			                                    $pass, 
			                                    $firstName, 
			                                    $lastName, 
			                                    $middleName, 
			                                    $birthDate,
			                                    $sex,
			                                    $token) 
		{
			try {
				$qry = $this->dbh->prepare("CALL UserRegister('$email', '$pass', '$firstName', '$lastName', '$middleName', '$birthDate', '$sex', '$token')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr[0];
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}	
		}
		
		/*Метод проверяет наличие email в базе*/
		public function checkEmail ($email) {
		    try {
				$qry = $this->dbh->prepare("CALL UserCheckEmail('$email')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr[0];
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}	
		    
		}
		
		/*Метод обновляет пароль пользователя после восстановления через форму*/
		public function restoreUserPassword ($email, $pass) {
			try {
				$qry = $this->dbh->prepare("CALL UserRestorePass ('$email', '$pass')");
				$qry->execute();		
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				//return $arr[0];
				return $arr[0];
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}	
		}
		
		
		/*Метод для авторизации пользователя*/
		public function authUser ($email, $password, $token) {
			try {	
				$pass = md5(md5(trim($password)));	
				$qry = $this->dbh->prepare("CALL UserLogin('$email', '$pass', '$token')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr[0];						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}				
		}
		
		
		/*Метод изменения пароля*/
		public function changePassword ($email, $pass, $oldPass, $token) {
		    try {	
				$qry = $this->dbh->prepare("CALL UserEditPassword('$email', '$pass',  '$token','$oldPass')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr[0];						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}	
		}
	
	    /*Проверка email*/
		public function checkRegisteredEmail($email, $token) {
		    try {	
				$qry = $this->dbh->prepare("CALL UserCheckRegistered('$email', '$token')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr[0];						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		
		/*Метод получает сервисы из базы по serviceId*/
		public function getServiceByServiceId ($s_id) {
		    try {	
				$qry = $this->dbh->prepare("CALL GetServiceByServiceId('$s_id')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				if(empty($arr)) {
				    return array();
				} else {
				    return $arr;
				}
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		/*Метод получает сервисы из базы по serviceId*/
		public function getServiceByClinicId ($cl_id) {
		    try {	
				$qry = $this->dbh->prepare("CALL GetServiceByClinicId('$cl_id')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr;						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		
		/*Метод получает сервисы из базы по parentId*/
		public function getServiceByParentId ($p_id) {
		    try {	
				$qry = $this->dbh->prepare("CALL GetServiceByParentId('$p_id')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr;						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
	
		/*Метод получает сервисы из базы*/
		public function getClinicsByClinicId ( $cl_id) {
		    try {	
				$qry = $this->dbh->prepare("CALL GetClinicsByClinicId( '$cl_id')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				if(empty($arr)) {
				    return array();
				} else {
				    return $arr;
				}						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		/*Метод получает клиники из базы по serviceId*/
		public function getClinicsByServiceId ($s_id) {
		    try {	
				$qry = $this->dbh->prepare("CALL GetClinicsByServiceId('$s_id')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				if(empty($arr)) {
				    return array();
				} else {
				    return $arr;
				}						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		 /*Проверка email*/
		public function checkToken($email, $token) {
		    try {	
				$qry = $this->dbh->prepare("CALL UserCheckToken('$email', '$token')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr[0];						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		/*CallMe*/
		public function callMe($phone) {
		    try {	
				$qry = $this->dbh->prepare("CALL CallMe('$phone')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr[0];						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		
		//Побуем вставить параметры в табличку measureTypes
		public function insertMedicalParams($param) {
		    try {	
		        //$qry_str = "INSERT IGNORE INTO measureType (shortName) VALUES $str_par;";
		        
		        //$new_str = "INSERT INTO measureType (shortName) VALUES $str_par ON DUPLICATE KEY UPDATE mTypeId=LAST_INSERT_ID(mTypeId), shortName=VALUES(shortName);";
		        
				$qry = $this->dbh->prepare("CALL InsertMesType('$param')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr;
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		//Метод для вставки новых HEalthKit аллиасов 
		public function insertMedicalParamAlias ($param, $appType) {
		    
		    try {
		        $qry = $this->dbh->prepare("CALL InsertMesTypeAlias('$param', '$appType')");
		        $qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr;
		    } catch (PDOException $e) {
		        echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
		    }
	    
		}
		
		
		
		//Побуем вставить параметры в табличку measureTypes
		public function insertMedicalParamsNew($str_param) {
		    try {	
		        $qry_str = "INSERT INTO medical_cards (user_id, mesType, value) VALUES $str_param;";
				$qry = $this->dbh->prepare($qry_str );
				$qry->execute();
				return $this->dbh->lastInsertId();
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		
		//Побуем вставить параметры в табличку measureTypes
		public function insertMedicalParamsWithDate($str_param) {
		    try {	
		        $qry_str = "INSERT IGNORE INTO medical_cards (user_id, mesType, value, dateEdit, appType) VALUES $str_param;";
				
				$qry = $this->dbh->prepare($qry_str );
				$qry->execute();
				return $this->dbh->lastInsertId();
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		
		
		//Побуем вставить параметры в табличку measureTypes
		public function editUserBirth($email, $date) {
		    try {	
		        //$qry_str = "INSERT IGNORE INTO measureType (shortName) VALUES $str_par;";
		        
		        //$new_str = "INSERT INTO measureType (shortName) VALUES $str_par ON DUPLICATE KEY UPDATE mTypeId=LAST_INSERT_ID(mTypeId), shortName=VALUES(shortName);";
		        
				$qry = $this->dbh->prepare("UPDATE users SET birthDate = '$date' WHERE email = '$email'");
				$qry->execute();
				return 1;
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
			//Побуем вставить параметры в табличку measureTypes
		public function editUserFirstName($email, $name) {
		    try {	
		        //$qry_str = "INSERT IGNORE INTO measureType (shortName) VALUES $str_par;";
		        
		        //$new_str = "INSERT INTO measureType (shortName) VALUES $str_par ON DUPLICATE KEY UPDATE mTypeId=LAST_INSERT_ID(mTypeId), shortName=VALUES(shortName);";
		        
				$qry = $this->dbh->prepare("UPDATE users SET firstName = '$name' WHERE email = '$email'");
				$qry->execute();
				return 1;
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		
			//Побуем вставить параметры в табличку measureTypes
		public function editUserLastName($email, $name) {
		    try {	
		        //$qry_str = "INSERT IGNORE INTO measureType (shortName) VALUES $str_par;";
		        
		        //$new_str = "INSERT INTO measureType (shortName) VALUES $str_par ON DUPLICATE KEY UPDATE mTypeId=LAST_INSERT_ID(mTypeId), shortName=VALUES(shortName);";
		        
				$qry = $this->dbh->prepare("UPDATE users SET lastName = '$name' WHERE email = '$email'");
				$qry->execute();
				return 1;
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		
			//Побуем вставить параметры в табличку measureTypes
		public function editUserMiddleName($email, $name) {
		    try {	
				$qry = $this->dbh->prepare("UPDATE users SET middleName = '$name' WHERE email = '$email'");
				$qry->execute();
				return 1;
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
				//Побуем вставить параметры в табличку measureTypes
		public function editUserSex($email, $name) {
		    try {	
				$qry = $this->dbh->prepare("UPDATE users SET sex = '$name' WHERE email = '$email'");
				$qry->execute();
				return 1;
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		
		
		//Побуем вставить  параметры в табличку medical_cards
		public function insertMedicalValue($type, $val, $email) {
		    try {	
		        $qry_str = "CALL UserEditMedParam('$type', '$val', '$email')";
				$qry = $this->dbh->prepare($qry_str);
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr[0];
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		
		/*Получить медкарту пользователя*/
		public function getMedicalInfo ($email) {
		    try {	
				$qry = $this->dbh->prepare("CALL UserMedicalInfo('$email')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);

                
				return $arr;						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		/*Изменить медкарту пользователя*/
		public function editMedicalInfo ($email, $token) {
		    try {	
				$qry = $this->dbh->prepare("CALL UserMedicalEdit('$email', '$token')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr[0];						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		//Пробуем вытащить даты изменений мед карты в массив
        public function getDatesMedCard ($email) {
            try {	
				$qry = $this->dbh->prepare("CALL GetMedDates('$email')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_KEY_PAIR);
				return $arr;						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
        }
        
        //Метод получает типа данных 
        public function getMedTypes ($type) {
            try {	
				$qry = $this->dbh->prepare("SELECT shortName, name, typeValue AS type, minVal, maxVal 
				                            FROM measureType WHERE enableType='yes' AND typeData='$type' ORDER BY sort DESC;");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr;						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
            
        }
        
        
		
		/*Пробуем получить медкарту поьзователя*/
		public function getMedCard ($email) {
		    try {	
				$qry = $this->dbh->prepare("CALL UserGetMedCard('$email')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr;						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		
		//userLogout
		public function userLogout ($email, $token) {
            try {	
				$qry = $this->dbh->prepare("CALL UserLogout('$email', '$token')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr[0];						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		} 
		
		//Удалить профиль
		public function delProfile( $email, $token) {
            try {	
				$qry = $this->dbh->prepare("CALL UserRemoveProfile('$email', '$token')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr[0];						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		} 
		
		
		//Метод повторной активации профиля
		public function reActivate ($email, $token) {
		    try {	
				$qry = $this->dbh->prepare("CALL UserReActivate('$email', '$token')");
				$qry->execute();
				$arr = $qry->fetchAll(PDO::FETCH_ASSOC);
				return $arr[0];						
			} catch (PDOException $e) {
				echo "Error!: " . $e->getMessage() . "<br/>";
				return $e->getMessage();
			}
		}
		
		
}


?>