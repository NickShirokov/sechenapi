//Файл для работы с текущими скриптами
$(document).ready(function(){
    
    
   $(window).scroll(function(){
       
       if ($(window).scrollTop() > 0) {
           $('.navbar').css('background', 'rebeccapurple');
       } else {
           $('.navbar').css('background', 'transparent');
       }
    });
    
    //Прокрутка
    jQuery('a[href^="#"]').click(function(){
        
        $('.navbar li').removeClass('active');
        var li = $(this).parent();
        //scrolling ();
        var el = jQuery(this).attr('href');
        jQuery('body').stop().animate({
            scrollTop: jQuery(el).offset().top - 115}, 1000,
            function() {
                $('.navbar li').removeClass('active');
                li.addClass('active');
                
               
            }
            
            
            );
            //scrollTop: jQuery(el).offset().top - 75}, 1000);
            //return false;
        });
    //Popup    
    $('.op-pop').click(function(){
        
        $('.suc-msg').remove();
        $('.inv-msg').remove();
        jQuery('.popup-body h2, #form1').css('display', 'block');
        $('.popup').show();
        
    });
    
    $('.close-popup').click(function(){
        $('.popup').hide();
    });
    
    
    //Маска телефона
    jQuery(function($){
       $(".phone").mask("+7 (999) 999-99-99");
       
         jQuery('.phone').focus(function(){
            jQuery(this).attr('placeholder', '');
        });
    });
    
   
    
    //Маска телефона
    jQuery(function($){
       $(".time").mask("99:99");
    });
    
    
    //Кнопки да нет
    $('.yn').click(function() {
        if ($(this).hasClass('active')) {
            //$(this).removeClass('active');
        } else {
            $('.yn').removeClass('active');
            $(this).addClass('active');
        }       
    });
    
    

});

/*Функция редиректа в доп. окно*/
function newWindowRedirect(url) {	
	window.open(url,'name','width=990,height=630,left=300,top=100,scrollbars=0'); 	
}

	/*Переход на новую страницу по url*/
function windowRef(url){ 		
	window.location = url;
} 


/*Функция возвращает полный урл главной страницы с протоколом*/
function urlWithProtocol () {
	var url = window.location.protocol+"//"+window.location.hostname;	
	return url;
}



//
function ajaxForm () {
    
    var url = urlWithProtocol ();
    var msg   = jQuery('#form1').serialize();
    jQuery('.inv-msg').remove();

	jQuery.ajax({
		type: "POST",          
		url: url+"/ajax/form1.php",
		data: msg,
		success: function(data) {
		    
			jQuery('.load-content').css('display', 'none');				
			result = jQuery.parseJSON(data);	
			if (result['errorCode'] == 1) {		
			    
    			jQuery('.popup-body .nm').before('<span class="inv-msg">'+result['error']['name']+'</span>');
    			jQuery('.popup-body .phone').before('<span class="inv-msg">'+result['error']['phone']+'</span>');
    			
    			jQuery('.popup-body input').focus(function(){
    			    
    			    jQuery('.inv-msg').remove();
    			});
    				
			} else {	
                jQuery('.popup-body h2, #form1').css('display', 'none');
                jQuery('.popup-body .close-popup').after('<h2 class="suc-msg">'+result['msg']+'</h2>');
			}	
		},
		error: function () {
		    
		    alert ('При отправке данных произошли ошибки!');
		}
	});
}



